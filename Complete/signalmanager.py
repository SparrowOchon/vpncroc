import signal
import processmanager


class SignalManager:
    """
    Manager of all Unix signals towards the process.
        Note: This is a Singleton
    """

    __instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(SignalManager, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    @staticmethod
    def monitor():
        """
	Monitor for all Unix Signals that are passed to the process
	"""
        signal.signal(signal.SIGHUP, SignalManager.handler)
        signal.signal(signal.SIGINT, SignalManager.handler)
        signal.signal(signal.SIGQUIT, SignalManager.handler)
        signal.signal(signal.SIGILL, SignalManager.handler)
        signal.signal(signal.SIGSEGV, SignalManager.handler)
        signal.signal(signal.SIGPWR, SignalManager.handler)
        signal.signal(signal.SIGTERM, SignalManager.handler)

    @staticmethod
    def handler(signum, frame):
        """
        Handle signals

        @param signum {Integer}: Signal number that triggered the handler
        @param frame {Signal Frame}: Stack frame at the time of signal
        """
        processmanager.revert_changes()
