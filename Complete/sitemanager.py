import re
from enum import Enum
import dryscrape
from bs4 import BeautifulSoup
from requests import get
import base64

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36"


class VPNTYPE(Enum):
    UDP = 0
    TCP = 1


def get_proxy():
    """
    Fetch a working http proxy to use when making get requests.

    @return {Dictionary} List containing valid Http Proxy
    """
    try:
        proxy_url = (
            "https://www.proxy-list.download/api/v1/get?type=http&anon=elite&country=US"
        )
        proxy_response = (get(proxy_url).text).split("\n", 1)[0]
        return {"http": "http://{proxy_response}"}
    except Exception as e:
        raise Exception(f"Error: -Get_Proxy {e}")


def get_available_vpn_list(proxy_url):
    """
    Navigate to target website fetch list of all ONLINE vpns and return them.
    NOTE: Ignore Premium Servers.

    @return: {Array of Strings} List of all ONLINE vpns.
    """
    try:
        url = base64.b64decode("aHR0cHM6Ly93d3cuZnJlZW9wZW52cG4ub3JnL2VuLw==").decode(
            "utf-8"
        )
        online_vpn_list = []
        headers = {"User-Agent": USER_AGENT}
        main_html = get(url, headers=headers, proxies=proxy_url).text
        soup = BeautifulSoup(main_html, "html.parser")
        all_vpn_table = soup.find(
            "div",
            {"style": "width: 408px; height: 730px; float: left; margin-right: 20px;"},
        )  # No Ids or classes everything in nameless Divs to avoid automation
        for vpn in all_vpn_table.prettify().split(
            '<div style="float: left; width: 187px; height: 55px; margin: 0 2px 5px 2px; background: #e1eaea;">'
        ):
            if "ONLINE" not in vpn:
                continue
            elif "* Premium servers" in vpn:
                break
            else:
                online_vpn_list.append(
                    url + re.search(r"logpass\/.*.php", vpn).group(0)
                )
        return online_vpn_list
    except Exception as e:
        raise Exception(f"Error: -Fetching VPN-List: {e}")


def get_vpn_info_block(online_vpn):
    """
    Fetch fetch the Div containing the Download links for the config
    And the User and PassImage

    @param online_vpn: {String} Vpn whose info page we would like to visit.
    @return: {String} The information page of the wanted vpn.
    """
    try:
        session = dryscrape.Session()
        session.visit(online_vpn)
        active_vpn_html = session.body()
        soup = BeautifulSoup(active_vpn_html, "html.parser")
        return soup.find("div", {"style": "margin: 28px 0;"})
    except Exception as e:
        raise Exception(f"Error: -Fetching VPN-Info: {e}")


def get_vpn_pass_image(working_dir_path, table, referer_url, proxy_url):
    """
    Look through the table for the captcha password image and save it locally.

    @param working_dir_path: {String} Url of the page that hosts information about the VPN I.e
    hosts user and password captcha.

    @param table: {Beautifulsoup Object} Table that contains the div with the password captcha.
    @param referer_url: {String} url string of the referer to this page.
    @return: {String} Local captcha file Path.
    """
    try:
        password_location = f"{working_dir_path}/password.png"
        orc_image_url = re.findall('https.*?(?=")', table.text)[1]  # Link to Password
        par_data = (
            re.compile(r"[^\?]+$").search(orc_image_url).group(0)
        )  # Fetch Dynamically generated string.
        cookie = {"par": par_data}  # Automation prevention bypass
        header = {
            "Referer": referer_url,
            "User-Agent": USER_AGENT,
        }  # Automation prevention bypass
        response = get(orc_image_url, cookies=cookie, headers=header, proxies=proxy_url)
        if response.status_code == 200:
            with open(password_location, "wb+") as opened_password_file:
                opened_password_file.write(response.content)
        else:
            print("Unable to connect to VPN page")
        return password_location
    except Exception as e:
        raise Exception(f"Error: -Fetching Vpn-Image: {e}")


def get_vpn_config(working_dir_path, table, vpn_type, proxy_url):
    """
    Download the .opvn config file for the VPN associated with the type we will be using I.e UDP or TCP and store it
    locally.

    @param working_dir_path: {String} Url of the page that hosts information about the VPN
    I.e hosts user and password captcha.

    @param table: {Beautifulsoup Object} Table that contains the div with the
    .opvn config urls.

    @param vpn_type: {Integer} Type of vpn connection we want to use I.e UDP or TCP
    @return: {String} Local openvpn config path.
    """
    try:
        config_location = f"{working_dir_path}/vpn.ovpn"
        vpn_config_url = table.find_all("a")[vpn_type.value].get(
            "href"
        )  # Link to both UDP and TCP to download
        headers = {"User-Agent": USER_AGENT}
        response = get(vpn_config_url, headers=headers, proxies=proxy_url)
        if response.status_code == 200:
            with open(config_location, "wb+") as opened_vpn_config:
                opened_vpn_config.write(response.content)
        else:
            print("Unable to connect to VPN page")
        return config_location
    except Exception as e:
        raise Exception(f"Error: -Fetching Vpn-Config: {e}")
