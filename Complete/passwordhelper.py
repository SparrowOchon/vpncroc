import pickle
import os
from keras.models import load_model
import numpy as np
import imutils
import cv2

dir_path = os.path.dirname(os.path.realpath(__file__))
MODEL_FILENAME = f"{dir_path}/captchamodel.hdf5"
MODEL_LABELS_FILENAME = f"{dir_path}/modellabels.dat"


def resize_image(image, width, height):
    """
    Resize the image to passed in width and height values. Do so by setting the larger of
    the initial two back to the desired values and then add padding to the re-sizing a second
    time to eliminate rounding issues.


    @param image: Initial image which we wish to resize
    @param width: {Integer} Width target of final image
    @param height: {Integer} Height target of final image
    @return: Resized image of dimensions param width x param height.
    """
    try:
        (h, w) = image.shape[:2]
        if w > h:  # Only resize one to avoid quality loss.
            image = imutils.resize(image, width=width)
        else:
            image = imutils.resize(image, height=height)
        pad_width = int((width - image.shape[1]) / 2.0)
        pad_height = int((height - image.shape[0]) / 2.0)
        image = cv2.copyMakeBorder(
            image, pad_height, pad_height, pad_width, pad_width, cv2.BORDER_REPLICATE
        )
        image = cv2.resize(image, (width, height))
        return image
    except Exception as e:
        raise Exception(f"Error: -Image-Resize: {e}")


def get_password_from_image(password_image):
    """
     Threshold the image and apply Morphological Transformations with OpenCV,
     split each image based on characters and leverage the trained CNN to identify each number.


    @param password_image: Initial unmodified image.
    @return: {String} Text in password image.
    """
    global MODEL_FILENAME
    global MODEL_LABELS_FILENAME
    try:
        NUMBERS_IN_CAPTCHA = 9  # Captcha Length is 9 numbers.
        INDIVIDUAL_NUMBER_SIZE = 1.25
        FINAL_NUMBER_WIDTH = 20
        FINAL_NUMBER_HEIGHT = 20

        model = load_model(MODEL_FILENAME)
        initial_image = cv2.imread(password_image)

        # Perform Morphing Transformations with OpenCV
        rectangular_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 1))
        closing_morph = cv2.morphologyEx(
            initial_image, cv2.MORPH_CLOSE, rectangular_kernel
        )
        opening_morph = cv2.morphologyEx(
            closing_morph, cv2.MORPH_OPEN, rectangular_kernel
        )
        rectangular_kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (2, 2))
        gradient_morph = cv2.morphologyEx(
            opening_morph, cv2.MORPH_GRADIENT, rectangular_kernel
        )

        # Split the gradient image into channels
        image_channels = np.split(np.asarray(gradient_morph), 3, axis=2)
        channel_height, channel_width, _ = image_channels[0].shape

        # Apply Otsu threshold to each channel
        for i in range(0, 3):
            _, image_channels[i] = cv2.threshold(
                ~image_channels[i], 0, 255, cv2.THRESH_OTSU | cv2.THRESH_BINARY
            )
            image_channels[i] = np.reshape(
                image_channels[i], newshape=(channel_height, channel_width, 1)
            )
        image = np.concatenate(
            (image_channels[0], image_channels[1], image_channels[2]), axis=2
        )

        # Greyscale the Image
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # Add some extra padding around the image
        gray_image = cv2.copyMakeBorder(
            gray_image, 30, 30, 30, 30, cv2.BORDER_REPLICATE
        )

        # Convert image to pure black and white
        black_and_white_image = cv2.threshold(
            gray_image, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU
        )[1]

        # Identify individual numbers
        number_contours = cv2.findContours(
            black_and_white_image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )
        number_contours = number_contours[1]

        number_image_location = []
        for contour in number_contours:
            # Get the rectangle that contains the contour
            (x, y, w, h) = cv2.boundingRect(contour)
            if w / h > INDIVIDUAL_NUMBER_SIZE:
                # Image is conjoined numbers
                half_width = int(w / 2)
                number_image_location.append((x, y, half_width, h))
                number_image_location.append((x + half_width, y, half_width, h))
            elif h >= 2 * w:
                # Image has ink bleed
                cut_height = int(w + 4)  # Compensate for the Bleeding
                number_image_location.append((x, y, w, cut_height))
            else:
                # This is a normal number by itself
                number_image_location.append((x, y, w, h))

        if len(number_image_location) != NUMBERS_IN_CAPTCHA:
            print("Unable to Read Captcha")
            return ""

        # Sort the detected number images based on the x coordinate i.e Left to Right
        number_image_location = sorted(number_image_location, key=lambda x: x[0])
        predictions = []

        with open(MODEL_LABELS_FILENAME, "rb") as opened_model_labels:
            label_model = pickle.load(
                opened_model_labels
            )  # Pickling labels model for prediction.

        for number in number_image_location:
            # Grab the coordinates of the number in the image
            x, y, w, h = number

            # Extract the number from the original image with a 2-pixel margin around the edge
            number_image = gray_image[y - 2 : y + h + 2, x - 2 : x + w + 2]

            # Re-size the number image to 20x20 pixels to match training data
            if number_image.size:
                number_image = resize_image(
                    number_image, FINAL_NUMBER_WIDTH, FINAL_NUMBER_HEIGHT
                )
            else:
                print("Number not loaded Correctly")
                return ""

            # Keras expects 4d list.
            number_image = np.expand_dims(number_image, axis=2)
            number_image = np.expand_dims(number_image, axis=0)

            # make prediction using NN
            prediction = model.predict(number_image)

            # Convert the one-hot-encoded prediction back to a normal number
            number = label_model.inverse_transform(prediction)[0]
            predictions.append(number)

        return "".join(predictions)
    except Exception as e:
        raise Exception(f"Error: -Image-To-Password: {e}")
