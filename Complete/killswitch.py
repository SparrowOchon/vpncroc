import socket
import os
import subprocess
import smtplib
from email.message import EmailMessage

USER_EMAIL = "user@mail.com"


def start(vpn_info, vpn_tunnel):
    """
    Set a list of Iptables rule(activate_kill) which prevents all unvpned communication.
        <PROTOCOL>:<IP>:<PORT>

    @params vpn_info {String} Vpn hostname and port
    @params vpn_protocol {List} The protocol to establish vpn against. I.e udp or tcp
    """
    try:
        vpn_info = vpn_info[0].split()
        vpn_hostname = socket.gethostbyname(vpn_info[0])
        vpn_port = vpn_info[1]
        vpn_protocol = vpn_info[2]

        dir_path = os.path.dirname(os.path.realpath(__file__))

        remote_call = subprocess.Popen(
            [
                "sudo",
                f"{dir_path}/killswitch.sh",
                f"{vpn_protocol}:{vpn_hostname}:{vpn_port}:{vpn_tunnel}",
            ],
            shell=False,
        ).wait()  # Get file path, pass in parameters for VPN_IP and VPN_PORT
    except Exception as e:
        raise Exception(f"Error: -Killswitch-Activation {e}")


def disable():
    """
    Flush all IPtables rules(reset_kill). Re-allowing normal connection.
    """
    dir_path = os.path.dirname(os.path.realpath(__file__))
    try:
        remote_call = subprocess.Popen(
            ["sudo", f"{dir_path}/killswitch.sh"], shell=False
        ).wait()
    except Exception as e:
        raise Exception(f"Error: -Killswitch-Deactivation {e}")


def notify(message):
    """
    Send an Email notification with the Error that occurred during script execution.

    @param message: {Exception} Exception that occurred during program runtime.
    """
    global USER_EMAIL
    try:
        email_message = EmailMessage()
        email_message.set_content(message)
        email_message["Subject"] = "SeedBox Notification."
        email_message["From"] = "notification@seedbox.com"
        email_message["To"] = USER_EMAIL

        smtp_server = smtplib.SMTP("smtp.server.address:25")  # With
        smtp_server.send_message(email_message)
        smtp_server.quit()
        print(f"Notification Sent: {message}")
    except Exception as e:
        print(f"Failed sending email: {e}")
