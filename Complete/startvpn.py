import tempfile
import os
import re
from multiprocessing import Process, Queue  # Avoid the GIL
import subprocess
import base64
import argparse
from requests import get
import sitemanager
import passwordhelper
import killswitch
import processmanager
import signalmanager

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "2"
# Global to allow for reverting changes even if force killed.

VPN_PREDICTIONS = 3  # Choose best prediction out of 3


def main():
    global VPN_PREDICTIONS

    cur_vpn = 0
    out_queue = Queue()
    vpn_type, user_proceses = get_user_parameters()
    processmanager.init_process(user_proceses)
    signalmanager.SignalManager.monitor()
    proxy_url = sitemanager.get_proxy()
    vpn_list = sitemanager.get_available_vpn_list(proxy_url)

    while cur_vpn < len(vpn_list):
        try:
            get_current_ip()
            # List of Online VPN Urls
            cur_vpn_page = sitemanager.get_vpn_info_block(
                vpn_list[cur_vpn]
            )  # Table info of the CUR_VPN number from the vpnList
            working_dir_path = tempfile.mkdtemp()  # WorkFolder
            auth_file_path = f"{working_dir_path}/auth.txt"
            try:
                PASSWORD_CREATION_THREAD = Process(
                    target=create_password_file,
                    args=(
                        working_dir_path,
                        auth_file_path,
                        cur_vpn_page,
                        vpn_list[cur_vpn],
                        proxy_url,
                    ),
                )
                PASSWORD_CREATION_THREAD.start()
                OPENVPN_CONFIG_THREAD = Process(
                    target=create_openvpn_config,
                    args=(
                        working_dir_path,
                        cur_vpn_page,
                        vpn_type,
                        auth_file_path,
                        out_queue,
                        proxy_url,
                    ),
                )
                OPENVPN_CONFIG_THREAD.start()
            except Exception as e:
                raise Exception(f"Error: -Threading: {e}")
            PASSWORD_CREATION_THREAD.join()
            OPENVPN_CONFIG_THREAD.join()
            print("Exiting Main Thread")
            vpn_config = out_queue.get()
            vpn_info = out_queue.get()
            vpn_protocol = out_queue.get()

            processmanager.start_openvpn(vpn_config, vpn_info, vpn_protocol)
            cur_vpn += 1

        except Exception as e:
            processmanager.revert_changes()
            killswitch.notify(e)


def get_current_ip():
    """
	Fetch current IP using ipify's api

	@return: {String} Current public IP address.
	"""
    try:
        initial_ip = get("https://api.ipify.org").text
        print("Current IP address: " + initial_ip)
        return initial_ip
    except Exception as e:
        raise Exception(f"Error: -Internet-Connection: {e}")


def create_password_file(
    working_dir_path, auth_file_path, cur_vpn_page, referer_url, proxy_url
):
    """
    Fetch the password image, guess its value and store it into a file.

    @param working_dir_path: {String} Temp file under which we will store the OPVN and password image
    @param cur_vpn_page: {Beautiful Object} Page containing the OPVN file aswell as the user and pass image
    @param referer_url: {String} Parent url of the page hosting the password image
    @param proxy_url: {Dictionary} Proxy list used when making get requests
  """
    try:
        password_list = []
        final_password = ""

        original_url = base64.b64decode("ZnJlZW9wZW52cG4=").decode("utf-8")

        # If the Image getting Pulled is not readable Re-Read it
        while not final_password:
            image_location = sitemanager.get_vpn_pass_image(
                working_dir_path, cur_vpn_page, referer_url, proxy_url
            )
            for x in range(VPN_PREDICTIONS):
                # Read the Image VPN_PREDICTION Times to confirm its the correct Text
                password = passwordhelper.get_password_from_image(image_location)
                if password not in password_list:
                    password_list.append(password)
                else:
                    final_password = password  # best 2 out of 3
                    break
        with open(auth_file_path, "w") as opened_auth_file:
            opened_auth_file.write(f"{original_url}\n")
            opened_auth_file.write(final_password)
        print("Password:" + final_password)
    except Exception as e:
        raise Exception(f"Error: -Password-Thread: {e}")


def create_openvpn_config(
    working_dir_path, cur_vpn_page, vpn_type, auth_file_path, out_queue, proxy_url
):
    """
    Modify the Vpn Config to automatically validate credentials aswell as setup
    killswitch rules to be used later based on openvpn_config

    @param working_dir_path: {String} Temp file under which we will store the OPVN and password image
    @param cur_vpn_page: {Beautiful Object} Page containing the OPVN file aswell as the user and pass image
    @param vpn_type: {Integer} Type of VPN connection we want to run I.e UDP/TCP
    @param auth_file_path: {String} File containing the Username and Password for the VPN.
    @param proxy_url: {Dictionary} Contains proxies used when making get requests
   """
    try:
        vpn_config = sitemanager.get_vpn_config(
            working_dir_path, cur_vpn_page, vpn_type, proxy_url
        )
        temp_file = tempfile.NamedTemporaryFile(delete=False)
        with open(vpn_config, "r") as opened_openvpn_config:
            vpn_config_txt = opened_openvpn_config.read()
        # =======================KILLSWITCH====================================
        remote_info = re.findall(r"^remote (.*)", vpn_config_txt, re.MULTILINE)
        remote_tunnel = re.findall(r"^dev (tun.*)", vpn_config_txt, re.MULTILINE)[0]
        # =====================================================================
        modified_config_file = vpn_config_txt.replace(
            "auth-user-pass",
            f"auth-user-pass {auth_file_path}\nconnect-retry 2\nconnect-retry-max 2\n",
        )
        with open(temp_file.name, "w") as opened_temp_config:
            opened_temp_config.write(modified_config_file)
        out_queue.put(temp_file.name)
        out_queue.put(remote_info)
        out_queue.put(remote_tunnel)
    except Exception as e:
        raise Exception(f"Error: -Config-Thread: {e}")


def get_user_parameters():
    """
    Get all users inputs passed as parameters
    @return: {VPNTYPE ENUM} Type of Connection the users wants to establish
    @return: {List} List of High Value Processes we want to monitor and kill on VPN death.
    """
    vpn_protocol = sitemanager.VPNTYPE.UDP
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        "--vpnprotocol",
        help="Vpn Protocol < UDP | TCP >",
        choices=["udp", "tcp"],
        required=True,
    )
    parser.add_argument(
        "-p",
        "--process",
        help="List of High Value Proceeses to Watch",
        nargs="+",
        required=True,
    )
    args = parser.parse_args()

    if args.vpnprotocol == "tcp":
        vpn_protocol = sitemanager.VPNTYPE.TCP
    elif args.vpnprotocol == "udp":
        vpn_protocol = sitemanager.VPNTYPE.UDP
    process_list = validate_service(args.process)
    return vpn_protocol, process_list


def validate_service(path):
    """
    Validate that the passed in service name or executable exists in the system
    @param path {String}: Path to process/service name we are interested in checking]
    @param process_list {Object of High_Value_Process}:

    """
    process_list = processmanager.High_Value_Process()

    for highval_process in path:
        try:
            output = subprocess.check_output(
                ["sudo", "systemctl", "cat", highval_process], stderr=subprocess.STDOUT
            )
            process_list.system_process.append(highval_process)
            continue
        except subprocess.CalledProcessError as e:
            pass

        process_path = validate_path(highval_process)
        process_list.user_exec.append(os.path.abspath(process_path))
    return process_list


def validate_path(file_path):
    """
    Validate if a file_path is a valid present path on the system

    @param file_path {String}: Path to file we want to validate
    @return {Bool} Pressence of Path to file
    """
    if os.path.exists(file_path):
        return file_path
    else:
        raise argparse.ArgumentTypeError("%s is an invalid file path" % file_path)


if __name__ == "__main__":
    main()

# Restart Script
