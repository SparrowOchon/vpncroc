import os
import subprocess
import killswitch


class High_Value_Process:
    system_process = []
    user_exec = []


OPENVPN_PATH = "/usr/sbin/openvpn"
OPENVPN_PROCESS = 0
PROCESS_LIST = High_Value_Process()


def init_process(process_list):
    global PROCESS_LIST
    PROCESS_LIST = process_list


def start_process():
    """
    Start a list of high valued processes store the started processes in processes.

    @param monitored_process_list {List}: List of High value process names or paths
    """
    loop_index = 0
    try:
        for process_name in PROCESS_LIST.user_exec:
            if os.path.exists(process_name):
                active_process = subprocess.Popen([process_name], shell=False)
                PROCESS_LIST.user_exec[loop_index] = active_process
                # System service
            else:
                raise Exception(f"Invalid user executable: {process_name}")
            loop_index += 1

        loop_index = 0
        for process_name in PROCESS_LIST.system_process:
            active_process = subprocess.Popen(
                ["sudo", "systemctl", "start", process_name], shell=False
            )
            PROCESS_LIST.system_process[loop_index] = active_process
            loop_index += 1
    except Exception as e:
        raise Exception(f"Error: -Starting-HighValued Processes {e}")


def stop_process():
    """
    Stop all currently running High valued Processes
    """
    try:
        system_processes = [
            subprocess.Popen(f"sudo systemctl stop {process_name}", shell=True)
            for process_name in PROCESS_LIST.system_process
        ]
        for process_name in PROCESS_LIST.user_exec:
            process_name.terminate().wait()  # Wait for exit

        for active_process in system_processes:
            active_process.wait()
    except AttributeError as e:
        pass
    except Exception as e:
        raise Exception(f"Error: -Process-Killing {e}")


def revert_changes():
    """
    Revert all changes. I.e kill the processes, the VPN and then disable the killswitch

    GLOBAL PARAMS
    @param processmanager {Object}: Object of type ProcessManager
    @param OPENVPN_PROCESS {Object}: Popen Object of running OpenVPN process
    @param killswitch {Object}: Object of type Killswitch
    """
    global OPENVPN_PROCESS
    try:
        stop_process()
    except NameError:
        pass
    try:
        OPENVPN_PROCESS.kill()
    except (OSError, NameError):
        print("Warning: OpenVPN not running. Thus not terminated")
    killswitch.disable()


def start_openvpn(vpn_config, vpn_info, remote_protocol):
    """
    Try launching Openvpn if it succeeds launch the killswitch and the
    High valued Processes, if it fails revert changes.

    @param vpn_config {String}: Location of VPN config
    @param vpn_info {List}: VPN info that was added to activate_kill.sh
    @param monitored_process {List}: List of process names that we want to
            monitor as High valued Processes.
    """
    global OPENVPN_PROCESS
    try:
        OPENVPN_PROCESS = subprocess.Popen(
            ["sudo", OPENVPN_PATH, vpn_config],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
    except Exception as e:
        raise Exception(f"Error: -Openvpn-Start: {e}")
    try:
        while OPENVPN_PROCESS.poll() is None:
            out = OPENVPN_PROCESS.stdout.readline()
            if b"Initialization Sequence Completed" in out:
                print("Activating Vpn")
                killswitch.start(vpn_info, remote_protocol)
                start_process()
            elif b"All connections have been connect-retry-max" in out:
                print("Restarting VPN")
                revert_changes()
                break
            elif b"Enter Auth Username:" in out:
                print("VPN Config requires Credentials")
                break
            else:
                pass

    except Exception as e:
        raise Exception(f"Error: -Openvpn-Monitoring: {e}")
    revert_changes()
