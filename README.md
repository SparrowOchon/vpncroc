# VPNCroc

**NOTE: It should not be trusted with sensitive information as I hold no responsibility for leaks/breaches**
A VPN connector with a built in killswitch to prevent High Value Processes from being able to connect to the Internet without being Tunneled supports P2P.

### Features:

- Dynamic VPN fetching and Captcha solving using custom Trained Convolutional Neural Network
- Low execution time overhead
- Dynamic Error Recovery and Fault-Tolerance
- Realtime VPN Checking (No Timers)
- Notification of High Value Process errors/failures
- Notification System on Failure
- Easily modifiable
- Automatic Disabling of High Value Proccess on Kill attempt

### Machine Learning to Solve Captcha

A sample of just the neural net used is available as a POC [here](https://gitlab.com/SparrowOchon/CaptchaPOC)

### Installation

- Install pre-reqs: `apt-get install qt5-default libqt5webkit5-dev build-essential python3-lxml python3-pip xvfb`
- Install the requirements.txt **Recommended to use a Virtenv** `pip3 install -r requirements.txt`

### Using

- Run the startvpn.py `python3 startvpn.py -v <udp| tcp> -p <path/to/program | process.service>`
- Make sure the Machine is indeed secured by checking the public ip with curl `curl -L https://canihazip.com/s`
